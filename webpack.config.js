const path = require('path');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const PostCssPipelineWebpackPlugin = require('postcss-pipeline-webpack-plugin');
const postcss = require('postcss');
const csso = require('postcss-csso');
const criticalSplit = require('postcss-critical-split');

module.exports = {
  mode: 'production',
  entry: './src/js/main.js',
  output: {
    filename: '[name].js',
    path: path.resolve(__dirname, 'dist'),
  },
  module: {
    rules: [
      {
        test: /\.css$/,
        use: [
          MiniCssExtractPlugin.loader,
          'css-loader',
        ],
      },
    ],
  },
  plugins: [
    new MiniCssExtractPlugin(),
    // Stage #1: extract critical part of the styles
    new PostCssPipelineWebpackPlugin({
      predicate: css => {
        // We are interested only in the main styles.
        // So, let's skip all other generated files.
        return css === 'main.css';
      },
      processor: postcss([
        criticalSplit({
          output: criticalSplit.output_types.CRITICAL_CSS,
        }),
      ]),
      suffix: 'critical',
    }),
    // Stage #2: extract the rest of the styles
    new PostCssPipelineWebpackPlugin({
      predicate: css => {
        // We are interested only in the main styles.
        // So, let's skip al other generated files.
        return css === 'main.css';
      },
      processor: postcss([
        criticalSplit({
          output: criticalSplit.output_types.REST_CSS,
        }),
      ]),
      suffix: 'rest',
    }),
    // Stage #3: optimize generated files (main.critical.css, main.rest.css)
    new PostCssPipelineWebpackPlugin({
      predicate: css => {
        // Skip the main file. We son't distribute it.
        return css !== 'main.css';
      },
      processor: postcss([
        csso({
          restructure: false,
        }),
      ]),
      suffix: 'min',
      map: {
        inline: false,
      },
    })
  ],
};
